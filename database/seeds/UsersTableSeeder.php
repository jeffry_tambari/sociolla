<?php


use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->insert(['name' => 'aaa', 'email' => 'aaa@aaa.com','password' => '$2y$10$ez04ZiHPmyBUQ/wZ.qjfGOk/EkBCe5MANChbGCim52zOrsAv5CgpG','remember_token' => '1','role' => '1',]);
    }

}
