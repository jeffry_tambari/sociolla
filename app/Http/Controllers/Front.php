<?php

namespace App\Http\Controllers;

use App\Brand;
//use App\Category;
//use App\Product;
use App\User;
use App\Page1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
//use Cart;
//use Validator;
//use Input;

class Front extends Controller {

    var $brands;
    var $categories;
    var $products;
    var $title;
    var $description;
    var $users;
    var $pages;
    public function __construct() {
//        $this->brands = Brand::all(array('name'));
//        $this->categories = Category::all(array('name'));
//        $this->products = Product::all(array('id', 'name', 'price'));

//        $this->brands = User::all(array('name'));
    }

    public function index() {
        return Redirect::to('auth/login');
//        echo "<PRE>";
//        var_dump ( 424 );
//        echo "</PRE>";
//        exit;
    }

    public function users() {
        $this->users = User::all();

//        $books=Book::all();
//                return view('books.index',compact('books'));
        return view('users', array('users' => $this->users, 'title' => 'users', 'description' => '', 'page' => 'users'));

    }

    public function show($id){
        $user = User::find($id);
        return view('show', array('user' => $user, 'title' => 'Login', 'description' => '', 'page' => 'login'));
    }
    public function newuser(){
        return view('newuser', array( 'isNew' => 1, 'title' => 'Login', 'description' => '', 'page' => 'login'));
    }
    public function postnewuser(){

        if(Request::get('isNew')) {
            $user=Request::all();
            //User::create($user);
            User::create(['name' => Request::get('name'), 'email' => Request::get('email'), 'password' => bcrypt(Request::get('password')),'remember_token' => Request::get('remember_token'),'role' => Request::get('role'),]);

            return redirect('users');
            //return view('newuser', array( 'title' => 'Login', 'description' => '', 'page' => 'login'));
        } else {
            $userUpdate = Request::all();
            $user = User::find(Request::get('id'));
            //$user->update($userUpdate);

            if($user->password != Request::get('password')) {
                $user->update(['name' => Request::get('name'), 'email' => Request::get('email'), 'password' => bcrypt(Request::get('password')), 'remember_token' => Request::get('remember_token'), 'role' => Request::get('role'),]);
            } else {
                $user->update(['name' => Request::get('name'), 'email' => Request::get('email'), 'remember_token' => Request::get('remember_token'), 'role' => Request::get('role'),]);
            }
            return redirect('users');
        }
    }

    public function edituser($id){
        $user = User::find($id);
        return view('newuser', array( 'isNew' => 0, 'user' => $user, 'title' => 'Login', 'description' => '', 'page' => 'login'));
    }
    public function postedituser(){
//        $user=Request::all();
//        User::create($user);
//        return redirect('users');
//echo "<PRE>";
//var_dump ( 4242 );
//echo "</PRE>";
//exit;
//
//        $userUpdate=Request::all();
//        $user=User::find(Request::get('id'));
//        $user->update($userUpdate);
//        return redirect('users');

        //return view('newuser', array( 'title' => 'Login', 'description' => '', 'page' => 'login'));
    }
    public function deleteuser($id){
        User::find($id)->delete();
        return redirect('users');
//        $user = User::find($id);
//        return view('newuser', array( 'isNew' => 0, 'user' => $user, 'title' => 'Login', 'description' => '', 'page' => 'login'));
    }


    public function page($id) {

//        $this->pages = Page::all();
////        $books=Book::all();
////                return view('books.index',compact('books'));
//        return view('pages', array('pages' => $this->pages, 'title' => 'pages', 'description' => '', 'page' => 'pages'));
        $page = Page1::find($id);
        return view('showpage', array('page1' => $page, 'title' => 'Login', 'description' => '', 'page' => 'login'));

    }

    public function pages() {
        $this->pages = Page1::all();

//        $books=Book::all();
//                return view('books.index',compact('books'));
        return view('pages', array('pages' => $this->pages, 'title' => 'pages', 'description' => '', 'page' => 'pages'));

    }

    public function showpage($id){
        $page = Page1::find($id);
        return view('showpage', array('page' => $page, 'title' => 'Login', 'description' => '', 'page' => 'login'));
    }
    public function newpage(){
        return view('newpage', array( 'isNew' => 1, 'title' => 'Login', 'description' => '', 'page' => 'login'));
    }
    public function postnewpage(){
        if(Request::get('isNew')) {
            $page=Request::all();
            Page1::create($page);
            return redirect('pages');
            //return view('newpage', array( 'title' => 'Login', 'description' => '', 'page' => 'login'));
        } else {
            $pageUpdate = Request::all();
            $page = Page1::find(Request::get('id'));
            $page->update($pageUpdate);
            return redirect('pages');
        }
    }

    public function editpage($id){
        $page = Page1::find($id);
        return view('newpage', array( 'isNew' => 0, 'page1' => $page, 'title' => 'Login', 'description' => '', 'page' => 'login'));
    }
    public function deletepage($id){
        Page1::find($id)->delete();
        return redirect('pages');
//        $page = page::find($id);
//        return view('newpage', array( 'isNew' => 0, 'page' => $page, 'title' => 'Login', 'description' => '', 'page' => 'login'));
    }







//    public function login() {
//        return view('login', array('title' => 'Login', 'description' => '', 'page' => 'login'));
//    }

//    public function doLogin() {
////        return view('home');
////        return view('home', array('title' => 'Welcome', 'description' => '', 'page' => 'home', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
//        //return view('home', array('title' => 'Welcome', 'description' => '', 'page' => 'home'));
//echo "<PRE>";
//var_dump ( 13132 );
//echo "</PRE>";
//exit;
//
//        // validate the info, create rules for the inputs
//        $rules = array(
//            //'email'    => 'required|email', // make sure the email is an actual email
//            'username'    => 'required', // make sure the email is an actual email
//            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
//        );
//echo "<PRE>";
//var_dump ( 1313 );
//echo "</PRE>";
//exit;
//
//        if(Input::all()) {
//            echo "<PRE>";
//            var_dump(Input::all());
//            echo "</PRE>";
//exit;
//
//            // run the validation rules on the inputs from the form
//            $validator = Validator::make(Input::all(), $rules);
//    echo "<PRE>";
//    var_dump ( 333 );
//    echo "</PRE>";
//    exit;
//
//            // if the validator fails, redirect back to the form
//            if ($validator->fails()) {
//                return view('login', array('title' => 'Login', 'description' => '', 'page' => 'login'));
//
//    //            return Redirect::to('login')
//    //                ->withErrors($validator) // send back all errors to the login form
//    //                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
//
//            } else {
//
//                // create our user data for the authentication
//                $userdata = array(
//                    'username'     => Input::get('username'),
//                    'password'  => Input::get('password')
//                );
//
//                // attempt to do the login
//                if (Auth::attempt($userdata)) {
//
//                    // validation successful!
//                    // redirect them to the secure section or whatever
//                    // return Redirect::to('secure');
//                    // for now we'll just echo success (even though echoing in a controller is bad)
//                    echo 'SUCCESS!';
//
//                } else {
//
//                    // validation not successful, send back to form
//                    return Redirect::to('login');
//
//                }
//
//            }
//        } else {
//            return view('login', array('title' => 'Login', 'description' => '', 'page' => 'login'));
//
//        }
//
//
//    }
//
//
//    public function products() {
//        return view('products', array('title' => 'Products Listing', 'description' => '', 'page' => 'products', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
//    }

//    public function product_details($id) {
//        $product = Product::find($id);
//        return view('product_details', array('product' => $product, 'title' => $product->name, 'description' => '', 'page' => 'products', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
//    }
//
//    public function product_categories($name) {
//        return view('products', array('title' => 'Welcome', 'description' => '', 'page' => 'products', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
//    }
//
//    public function product_brands($name, $category = null) {
//        return view('products', array('title' => 'Welcome', 'description' => '', 'page' => 'products', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
//    }
//
//    public function blog() {
//        $posts = \App\Post::where('id', '>', 0)->paginate(3);
//        $posts->setPath('blog');
//
//        $data['posts'] = $posts;
//
//        return view('blog', array('data' => $data, 'title' => 'Latest Blog Posts', 'description' => '', 'page' => 'blog', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
//    }
//
//    public function blog_post($url) {
//        $post = \App\Post::where('url', '=', $url)->get();
//
//        $post_id = $post[0]['id'];
//
//        $tags = \App\BlogPostTag::postTags($post_id);
//
//        $previous_url = \App\Post::prevBlogPostUrl($post_id);
//        $next_url = \App\Post::nextBlogPostUrl($post_id);
//
//        $data['previous_url'] = $previous_url;
//        $data['next_url'] = $next_url;
//        $data['tags'] = $tags;
//        $data['post'] = $post[0];
//
//        return view('blog_post', array('data' => $data, 'title' => $post[0]['title'], 'description' => $post[0]['description'], 'page' => 'blog', 'brands' => $this->brands, 'categories' => $this->categories, 'products' => $this->products));
//    }
//
//    public function contact_us() {
//        return view('contact_us', array('title' => 'Welcome', 'description' => '', 'page' => 'contact_us'));
//    }
//
//    public function register() {
//        if (Request::isMethod('post')) {
//            User::create(['name' => Request::get('name'), 'email' => Request::get('email'), 'password' => bcrypt(Request::get('password')),]);
//        }
//
//        return Redirect::away('login');
//    }

    public function authenticate() {
        if (Auth::attempt(['email' => Request::get('email'), 'password' => Request::get('password')])) {
//            return redirect()->intended('checkout');
            return redirect()->intended('pages');
        } else {
            return view('login', array('title' => 'Welcome', 'description' => '', 'page' => 'home'));
        }


//        // validate the info, create rules for the inputs
//        $rules = array(
//            //'email'    => 'required|email', // make sure the email is an actual email
//            'username'    => 'required', // make sure the email is an actual email
//            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
//        );

//        if(Input::all()) {
//
//            // run the validation rules on the inputs from the form
//            $validator = Validator::make(Input::all(), $rules);
//
//
//            // if the validator fails, redirect back to the form
//            if ($validator->fails()) {
//
//
//                return view('login', array('title' => 'Login', 'description' => '', 'page' => 'login'));
//
//    //            return Redirect::to('login')
//    //                ->withErrors($validator) // send back all errors to the login form
//    //                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
//
//            } else {
//
//                // create our user data for the authentication
//                $userdata = array(
//                    'username'     => Input::get('username'),
//                    'password'  => Input::get('password')
//                );
//echo "<PRE>";
//var_dump ( 4141 );
//echo "</PRE>";
//exit;
//
//                // attempt to do the login
//                if (Auth::attempt($userdata)) {
//
//                    // validation successful!
//                    // redirect them to the secure section or whatever
//                    // return Redirect::to('secure');
//                    // for now we'll just echo success (even though echoing in a controller is bad)
//                    echo 'SUCCESS!';
//
//                } else {
//
//                    // validation not successful, send back to form
//                    //return Redirect::to('login');
//                    return view('login', array('title' => 'Login', 'description' => '', 'page' => 'login'));
//
//                }
//
//            }
//
//        }


    }

    public function login() {
        return view('login', array('title' => 'Welcome', 'description' => '', 'page' => 'home'));
    }

    public function logout() {
        Auth::logout();

        return Redirect::away('login');
    }

//    public function cart() {
//        //update/ add new item to cart
//        if (Request::isMethod('post')) {
//            $product_id = Request::get('product_id');
//            $product = Product::find($product_id);
//            Cart::add(array('id' => $product_id, 'name' => $product->name, 'qty' => 1, 'price' => $product->price));
//        }
//
//        //increment the quantity
//        if (Request::get('product_id') && (Request::get('increment')) == 1) {
//            $rowId = Cart::search(array('id' => Request::get('product_id')));
//
//            $item = Cart::get($rowId[0]);
//
//            Cart::update($rowId[0], $item->qty + 1);
//        }
//
//        //decrease the quantity
//        if (Request::get('product_id') && (Request::get('decrease')) == 1) {
//            $rowId = Cart::search(array('id' => Request::get('product_id')));
//            $item = Cart::get($rowId[0]);
//
//            Cart::update($rowId[0], $item->qty - 1);
//        }
//
//
//        $cart = Cart::content();
//
//        return view('cart', array('cart' => $cart, 'title' => 'Welcome', 'description' => '', 'page' => 'home'));
//    }
//
//    public function clear_cart() {
//        Cart::destroy();
//        return Redirect::away('cart');
//    }
//
//    public function checkout() {
//        return view('checkout', array('title' => 'Welcome', 'description' => '', 'page' => 'home'));
//    }
//
//    public function search($query) {
//        return view('products', array('title' => 'Welcome', 'description' => '', 'page' => 'products'));
//    }

}
