<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

//class Page extends Model //implements AuthenticatableContract
//{
//    protected $primaryKey = 'id';
class Page1 extends BaseModel {
    protected $primaryKey = 'id';
    protected $table = 'pages';
    protected $fillable = array('name', 'text');
    public $timestamps = false;
//    /**
//     * The database table used by the model.
//     *
//     * @var string
//     */
//    protected $table = 'pages';
//
//    /**
//     * The attributes that are mass assignable.
//     *
//     * @var array
//     */
////    protected $fillable = ['name', 'email', 'password'];
//    protected $fillable = ['name', 'text'];
//
//    /**
//     * The attributes excluded from the model's JSON form.
//     *
//     * @var array
//     */
////    protected $hidden = ['password', 'remember_token'];
}
