@extends('layouts.layout')

@section('content')
    <a href="{{url('newuser')}}">New User</a>
    {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}

    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr class="bg-info">
            <th>Id</th>
            <th>name</th>
            <th>email</th>
            <th>password</th>
            <th>remember_token</th>
            <th>role</th>
            <th colspan="3">Actions</th>
         </tr>
         </thead>
         <tbody>
         @foreach ($users as $user)
             <tr>
                 <td>{{ $user->id }}</td>
                 <td>{{ $user->name }}</td>
                 <td>{{ $user->email }}</td>
                 <td>{{ $user->password }}</td>
                 <td>{{ $user->remember_token }}</td>
                 <td>{{ $user->role }}</td>
                 <td>
                     <a href="{{url('edituser/' . $user->id )}}">Edit</a>
                     <a href="{{url('deleteuser/' . $user->id )}}" onclick="javascript:return confirm('Delete Sure?');">Delete</a>
                              {{--{!! Form::open(['method' => 'DELETE', 'route'=>['users.destroy', $user->id]]) !!}--}}
                              {{--{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}--}}
                              {{--{!! Form::close() !!}--}}
                  </td>
              </tr>
          @endforeach
          </tbody>
      </table>


@endsection