@extends('layouts.layout')

@section('content')
    <h1>{{ $isNew ? 'Create New User' : 'Edit User' }}</h1>
    <form method="post" action="{{url('newuser')}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="isNew" value="{{ $isNew }}">
        <input type="hidden" name="id" value="{{ @$user->id }}">
    <div class="form-group">
        Name
        <input type="text" name="name" value="{{ @$user->name }}" />
    </div>
    <div class="form-group">
        Email
        <input type="text" name="email" value="{{ @$user->email }}" />
    </div>
    <div class="form-group">
        Password
        <input type="text" name="password" value="{{ @$user->password }}" />
    </div>
    <div class="form-group">
        Remember token
        <input type="text" name="remember_token" value="{{ @$user->remember_token }}" />
    </div>
    <div class="form-group">
        Role
        <input type="text" name="role" value="{{ @$user->role }}" />
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-default">Submit</button>
    </div>
    </form>

@endsection