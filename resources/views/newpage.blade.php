@extends('layouts.layout')

@section('content')
    <h1>{{ $isNew ? 'Create New Page' : 'Edit Page' }}</h1>
    <form method="post" action="{{url('newpage')}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="isNew" value="{{ $isNew }}">
        <input type="hidden" name="id" value="{{ @$page1->id }}">
    <div class="form-group">
        Name
        <input type="text" name="name" value="{{ @$page1->name }}" />
    </div>
    <div class="form-group">
        Text
        <textarea name="text" >{{ @$page1->text }}</textarea>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-default">Submit</button>
    </div>
    </form>

@endsection