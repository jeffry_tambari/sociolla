<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="{{$description}}">
        <meta name="author" content="Rodrick Kazembe">
        <title>{{$title}} - Larashop</title>
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
{{--        <link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">--}}
{{--        <link href="{{asset('css/price-range.css')}}" rel="stylesheet">--}}
{{--        <link href="{{asset('css/animate.css')}}" rel="stylesheet">--}}
{{--        <link href="{{asset('css/main.css')}}" rel="stylesheet">--}}
        <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="{{asset('js/html5shiv.js')}}"></script>
        <script src="{{asset('js/respond.min.js')}}"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="{{asset('images/ico/favicon.ico')}}">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('images/ico/apple-touch-icon-144-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('images/ico/apple-touch-icon-114-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('images/ico/apple-touch-icon-72-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" href="{{asset('images/ico/apple-touch-icon-57-precomposed.png')}}">
    </head><!--/head-->

    <body>
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="social-icons pull-right">
                                <ul class="nav navbar-nav">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left">
                                <a href="{{url('')}}"><img src="{{asset('images/home/logo.png')}}" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                        </div>
                        <div class="mainmenu pull-left">
                            <ul class="nav navbar-nav collapse navbar-collapse">
                                <li><a href="{{Auth::check() ? url('users') : ''}}"> {{Auth::check() ? 'users' : ''}}</a></li>
                                <li><a href="{{url('pages')}}" {{$page == 'pages' ? 'class=active' : ''}}>Pages</a></li>
                                <li><a href="{{Auth::check() ? url('auth/logout') : url('auth/login')}}"><i class="fa fa-lock"></i> {{Auth::check() ? 'Logout' : 'Login'}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header><!--/header-->

        @yield('content')

        {{--<footer id="footer"><!--Footer-->--}}
            {{--<div class="footer-top">--}}
                {{--<div class="container">--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="footer-widget">--}}
                {{--<div class="container">--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="footer-bottom">--}}
                {{--<div class="container">--}}
                    {{--<div class="row">--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</footer><!--/Footer-->--}}



        <script src="{{asset('js/jquery.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
{{--        <script src="{{asset('js/jquery.scrollUp.min.js')}}"></script>--}}
{{--        <script src="{{asset('js/price-range.js')}}"></script>--}}
{{--        <script src="{{asset('js/jquery.prettyPhoto.js')}}"></script>--}}
{{--        <script src="{{asset('js/main.js')}}"></script>--}}
    </body>
</html>
