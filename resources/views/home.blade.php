@extends('layouts.layout')

@section('content')

    <link href="{{asset('css/login.css')}}" rel="stylesheet">


<div class="login">
	<h1>Login</h1>
    <form method="post">
    	<input type="text" name="username" placeholder="Username" required="required">
        <input type="password" name="password" placeholder="Password" required="required">
        <button type="submit" class="btn btn-primary btn-block btn-large">Let me in.</button>
    </form>
</div>



<section id="slider"><!--slider-->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div >

                </div>

            </div>
        </div>
    </div>
</section><!--/slider-->

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    {{--@include('shared.sidebar')--}}
                </div>
            </div>

        </div>
    </div>
</section>
@endsection