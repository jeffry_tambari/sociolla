@extends('layouts.layout')

@section('content')       
       <section id="form"><!--form-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-1">
                        <div class="login-form"><!--login form-->
                            <h2>Login to your account</h2>
                            <form method="POST" action="{{url('auth/login')}}">
                                {{--{!! csrf_field() !!}--}}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="email" name="email" id="email" placeholder="Email Address" />
                                <input type="password" name="password" id="password" placeholder="Password" />
                                {{--<span>--}}
                                    {{--<input name="remember" id="remember" type="checkbox" class="checkbox"> --}}
                                    {{--Keep me signed in--}}
                                {{--</span>--}}
                                <button type="submit" class="btn btn-default">Login</button>
                            </form>
                        </div><!--/login form-->
                    </div>
                </div>
            </div>
        </section><!--/form-->
@endsection