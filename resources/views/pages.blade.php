@extends('layouts.layout')

@section('content')
    <a href="{{url('newpage')}}">New Page</a>
    {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}

    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr class="bg-info">
            <th>Id</th>
            <th>name</th>
            <th>text</th>
            <th colspan="3">Actions</th>
         </tr>
         </thead>
         <tbody>
         @foreach ($pages as $page)
             <tr>
                 <td>{{ $page->id }}</td>
                 <td><a href="{{url('page/' . $page->id)}}" >{{ $page->name }}</a></td>
                 <td>{{ $page->text }}</td>
                 <td>
                     <a href="{{url('editpage/' . $page->id )}}">Edit</a>
                     <a href="{{url('deletepage/' . $page->id )}}" onclick="javascript:return confirm('Delete Sure?');">Delete</a>
                              {{--{!! Form::open(['method' => 'DELETE', 'route'=>['pages.destroy', $page->id]]) !!}--}}
                              {{--{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}--}}
                              {{--{!! Form::close() !!}--}}
                  </td>
              </tr>
          @endforeach
          </tbody>
      </table>


@endsection