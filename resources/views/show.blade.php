@extends('layouts.layout')

@section('content')
    <h1>User {{ $user->id }}</h1>
       <ul>
         <li>Name: {{ $user->name }}</li>
         <li>Email: {{ $user->email }}</li>
         <li>Role: {{ $user->role }}</li>
       </ul>
@endsection